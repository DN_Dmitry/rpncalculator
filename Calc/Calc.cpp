// Calc.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include<string>
#include<vector>
#include "RPNService.h"

extern "C"
{
	__declspec(dllexport) char* calculate(char* szInput)
	{
		RPNService rpn;
		std::string buf = rpn.resolveExpression(szInput);
		//std::vector<char> result(buf.begin(), buf.end());
		char* result = new char[buf.size()];
		strcpy_s(result, buf.size()+1, buf.c_str());
		return result;
	}
}
	

	