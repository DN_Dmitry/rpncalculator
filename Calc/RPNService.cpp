#include "stdafx.h"
#include "RPNService.h"


RPNService::RPNService()
{
	nIndex = 0;
}

inline char RPNService::nextChar() 
{
	if (nIndex < szInput.length()) 
	{
		return currentSymbol = szInput[nIndex++];
	}
	else return currentSymbol = '\0';
}

inline bool RPNService::isDigit(const char &c) 
{
	return ((c >= '0' && c <= '9') || c == '.');
}

inline int RPNService::prior(char c) 
{
	switch (c) {
	case '(': return 1;
	case '+': case '-': return 2;
	case '*': case '/': return 3;
	default: return 0;
	}
}

std::string RPNService::convertToRPN(const std::string &szSource)
{		
	std::string szResult;
	int nWasOperator = 0; int nWasBracket = 0; nIndex = 0;
	std::stack<char> stack;
	this->szInput = szSource;
	if ((!isDigit(szInput[0])) && szInput[0] != '(')
	{
		throw (char*)"Syntax error";
	}
	while (nextChar() != '\0') 
	{		
		if (isDigit(currentSymbol))
		{				
			szResult += currentSymbol;
			if (nIndex == szInput.size())
			{
				break;
			}
			if (nIndex < szInput.length())
			{
				while (isDigit(nextChar()))
				{
					szResult += currentSymbol;
				}
			}			
			nWasOperator = 0;
			nWasBracket = 0;
		}
		if(!isDigit(currentSymbol))
		{ 
			szResult += ' '; 
		}
		if (currentSymbol != '\0')
		{
			switch (currentSymbol)
			{
				case '(':
					stack.push(currentSymbol);
					++nWasBracket;
					nWasOperator = 0;
					break;
				case '*': case '/': case '+': case '-':
					if (nIndex == szInput.length())
						throw (char*)"Syntax error";

					if (!nWasOperator) {
						nWasOperator = 1;
						if (!stack.empty())
						{
							while (stack.size() > 0 && (prior(currentSymbol) <= prior(stack.top())))
							{
								szResult += stack.top();
								stack.pop();
							}
							if (stack.size() > 0 && (prior(currentSymbol) > prior(stack.top())))
							{
								stack.push(currentSymbol);
							}
							if (stack.empty())
							{
								stack.push(currentSymbol);
							}
						}
						else
						{
							stack.push(currentSymbol);
						}

						break;
					}
					else throw (char*)"Syntax error";

				case ')':
					if (nWasOperator)
					{
						throw (char*)"Syntax error";
					}
					else
					{
						while ((stack.top() != '('))
						{
							szResult += stack.top();
							stack.pop();
						}
						if (!stack.empty() && stack.top() == '(')
						{
							stack.pop();
						}
					}
					--nWasBracket;
					break;
				default:
				{
					throw (char*)"Error: invalid symbol in the string";
				}
			}
		}
		
	}
	if (!stack.empty())
	{
		while (!stack.empty())
		{
			szResult += stack.top();
			stack.pop();
		}
	}

	return szResult;
}


std::string RPNService::resolveExpression(const std::string &szRPNExpression)
{
	float nFirstOperand = 0; float nSecondOperand = 0; 
	szInput = convertToRPN(szRPNExpression);
	nIndex = 0;
	std::stack<std::string> stack;

	while (nextChar() != '\0')
	{
		if (isDigit(currentSymbol))
		{
			std::string item;
			item.push_back(currentSymbol);
			if (nIndex == szInput.size())
			{
				break;
			}
			if (nIndex < szInput.length())
			{
				while (isDigit(nextChar()))
				{
					item += currentSymbol;
				}
			}
			stack.push(item);
		}
		if (currentSymbol == '+' ||
			currentSymbol == '-' ||
			currentSymbol == '*' ||
			currentSymbol == '/')
		{
			nSecondOperand = strtof(stack.top().c_str(), 0);
			stack.pop();
			nFirstOperand = strtof(stack.top().c_str(), 0);
			stack.pop();
			switch (currentSymbol)
			{
				case '+': stack.push(std::to_string(nFirstOperand + nSecondOperand)); break;
				case '-': stack.push(std::to_string(nFirstOperand - nSecondOperand)); break;
				case '*': stack.push(std::to_string(nFirstOperand * nSecondOperand)); break;
				case '/': stack.push(std::to_string(nFirstOperand / nSecondOperand)); break;
			}			
		}
	}
	int numberOfDigitsInResult = 0;
	std::stringstream stream;
	std::cout << "stack.top()stack.top()stack.top()::: " + stack.top();
	bool isAfterDot = false;
	for (std::string::iterator it = stack.top().begin(); it != stack.top().end(); it++)
	{
		if (*(it) == '.')
		{			
			isAfterDot = true;
		}
		if (isAfterDot)
		{
			if (*(it) != '0') 
			{
				numberOfDigitsInResult++;
			}			
		}
	}
	stream << std::fixed << std::setprecision(numberOfDigitsInResult) << strtof(stack.top().c_str(), 0);
	return stream.str();
}



RPNService::~RPNService()
{
}
