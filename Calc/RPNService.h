#pragma once
#include<iostream>
#include<stack>
#include<string>
#include <sstream>
#include <iomanip>

class RPNService
{
	char currentSymbol;
	int nIndex;
	std::string szInput;

	inline char nextChar();
	inline char previousChar();
	inline bool isDigit(const char &c);
	inline int prior(char c);
	std::string convertToRPN(const std::string &szSource);
public:	
	RPNService();	
	std::string resolveExpression(const std::string &szRPNExpression);

	virtual ~RPNService();
};

