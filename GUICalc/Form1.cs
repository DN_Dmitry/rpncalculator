﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GUICalc
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();

            plus.Click += btnPlusClick;
            minus.Click += btnMinusClick;
            multiple.Click += btnPowClick;
            divide.Click += btnDivClick;
            openBracket.Click += btnOpBracketClick;
            closeBracket.Click += btnClsBracketClick;
            zero.Click += btnZeroClick;
            one.Click += btnOneClick;
            two.Click += btnTwoClick;
            three.Click += btnThreeClick;
            four.Click += btnFourClick;
            five.Click += btnFiveClick;
            six.Click += btnSixClick;
            seven.Click += btnSevenClick;
            eight.Click += btnEightClick;
            nine.Click += btnNineClick;
            equal.Click += btnEqualClick;
        }

        private void btnEqualClick(object sender, EventArgs e)
        {
            try
            {
                result.Text = Adapter.solve(input.Text);
            }
            catch(Exception ex)
            {
                string message = "I can't solve this. Check your input please.";
                string caption = "Something went wrong!";
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result;
                result = MessageBox.Show(message, caption, buttons);
                if (result == System.Windows.Forms.DialogResult.Yes)
                {
                    input.Text = null;
                }
                else
                {
                    this.Close();
                }
            }            
            input.Text = null;
        }

        private void btnPlusClick(object sender, EventArgs e)
        {
            input.Text += '+';
        }
        private void btnMinusClick(object sender, EventArgs e)
        {
            input.Text += '-';
        }
        private void btnPowClick(object sender, EventArgs e)
        {
            input.Text += '*';
        }
        private void btnDivClick(object sender, EventArgs e)
        {
            input.Text += '/';
        }
        private void btnOpBracketClick(object sender, EventArgs e)
        {
            input.Text += '(';
        }
        private void btnClsBracketClick(object sender, EventArgs e)
        {
            input.Text += ')';
        }

        private void btnZeroClick(object sender, EventArgs e)
        {
            input.Text += '0';
        }
        private void btnOneClick(object sender, EventArgs e)
        {
            input.Text += '1';
        }
        private void btnTwoClick(object sender, EventArgs e)
        {
            input.Text += '2';
        }
        private void btnThreeClick(object sender, EventArgs e)
        {
            input.Text += '3';
        }
        private void btnFourClick(object sender, EventArgs e)
        {
            input.Text += '4';
        }
        private void btnFiveClick(object sender, EventArgs e)
        {
            input.Text += '5';
        }
        private void btnSixClick(object sender, EventArgs e)
        {
            input.Text += '6';
        }
        private void btnSevenClick(object sender, EventArgs e)
        {
            input.Text += '7';
        }
        private void btnEightClick(object sender, EventArgs e)
        {
            input.Text += '8';
        }
        private void btnNineClick(object sender, EventArgs e)
        {
            input.Text += '9';
        }
    }
}
