﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;

namespace GUICalc
{
    public class Adapter
    {
        [DllImport("Calc.dll", EntryPoint = "calculate", CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr calculate([MarshalAs(UnmanagedType.LPStr)]String b);

        public static String solve(String toCalculate)
        {
            String result;
            try
            {
                result = Marshal.PtrToStringAnsi(calculate(toCalculate));
            }
            catch(Exception ex)
            {
                throw ex;
            }            
            return result;
        }

    }
}
